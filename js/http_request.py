def http_request(url: str, cb, cb_data_type, method, body):
    def _callback(resp):
        if cb_data_type == 'json':
            resp.json().then(lambda data: cb(data))
        elif cb_data_type == 'text':
            resp.text().then(lambda data: cb(data))
        else:
            resp.blob().then(lambda data: cb(data))
    params = {'method': method}
    if body:
        params['body'] = body
    fetch(url, params).then(_callback)


async def async_http_request(url: str, cb_data_type, method, body):
    def _resolution(res, _):
        http_request(url, res, cb_data_type, method, body)

    return __new__(Promise(_resolution))
