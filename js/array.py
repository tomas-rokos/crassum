# __pragma__('noskip')


def sort_array_by_lambda(arr, fn):
    arr.js_sort(fn)


def is_array_equal(a, b):
    if len(a) != len(b):
        return False
    for i in range(len(a)):
        if a[i] != b[i]:
            return False
    return True


# __pragma__('skip')
from functools import cmp_to_key  # noqa E402


def sort_array_by_lambda(arr, fn):
    arr.sort(key=cmp_to_key(fn))


def is_array_equal(a, b):
    return a == b
