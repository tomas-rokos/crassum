# __pragma__('skip')


class window:
    onpopstate = None


# __pragma__('noskip')

cbs = []


def _notify():
    new_loc = get_location()
    for cb in cbs:
        cb(new_loc)


def get_location():
    return window.location


def navigate(href: str):
    window.history.pushState(None, None, href)
    _notify()


def on_navigation(cb):
    cbs.append(cb)


window.onpopstate = _notify


# __pragma__('skip')


from collections import namedtuple


def get_location():
    return namedtuple('location', ('pathname'))('/index')


def navigate(href: str):
    pass


def on_navigation(cb):
    pass
