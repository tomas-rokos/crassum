from crassum.utils.random_string import random_string

globally_registered_styles = {}


def register_style(val: str) -> str:
    already = globally_registered_styles.get(val)
    if already:
        return already
    new_key = 'registered-' + random_string(10)
    globally_registered_styles[val] = new_key
    style = document.createElement('style')
    style.type = 'text/css'
    val = val.replace(':host', '.' + new_key)
    style.appendChild(document.createTextNode(val))
    document.head.appendChild(style)
    return new_key
