
class IndexedDBObjectStore:
    def __init__(self, idb_object_store):
        self.idb_object_store = idb_object_store

    async def put(self, doc, doc_key=None):
        def _resolution(res, _):
            if doc_key is None:
                rq = self.idb_object_store.put(doc)
            else:
                rq = self.idb_object_store.put(doc, doc_key)

            def _idb_cb(event):
                res(event.target.result)
            rq.onsuccess = _idb_cb
        return __new__(Promise(_resolution))

    async def delete(self, doc_id):
        def _resolution(res, _):
            rq = self.idb_object_store.delete(doc_id)

            def _idb_cb(event):
                res(event.target.result)
            rq.onsuccess = _idb_cb
        return __new__(Promise(_resolution))

    async def get(self, doc_id):
        def _resolution(res, _):
            rq = self.idb_object_store.js_get(doc_id)

            def _idb_cb(event):
                res(event.target.result)
            rq.onsuccess = _idb_cb
        return __new__(Promise(_resolution))

    async def get_all(self):
        def _resolution(res, _):
            rq = self.idb_object_store.getAll()

            def _idb_cb(event):
                res(event.target.result)
            rq.onsuccess = _idb_cb
        return __new__(Promise(_resolution))

    async def get_single_key_value(self, index_name, key):
        def _resolution(res, _):
            bound = IDBKeyRange.only(key)
            rq = self.idb_object_store.index(index_name).openCursor(bound)
            result = []

            def _idb_cb(event):
                cursor = event.target.result
                if cursor:
                    result.append((cursor.primaryKey, cursor.value))
                    __pragma__('js', 'cursor.continue();')
                else:
                    if len(result) != 1:
                        print('PROBLEM, no single key value', result)
                    res(result[0])
            rq.onsuccess = _idb_cb
        return __new__(Promise(_resolution))

    async def get_range(self, index_name, start, include_start, end, include_end):
        def _resolution(res, _):
            bound = IDBKeyRange.bound(start, end, not include_start, not include_end)
            rq = self.idb_object_store.index(index_name).openCursor(bound)
            result = []

            def _idb_cb(event):
                cursor = event.target.result
                if cursor:
                    result.append(cursor.value)
                    __pragma__('js', 'cursor.continue();')
                else:
                    res(result)
            rq.onsuccess = _idb_cb
        return __new__(Promise(_resolution))

    async def get_primary_keys(self, index_name, start, include_start, end, include_end):
        def _resolution(res, _):
            bound = IDBKeyRange.bound(start, end, not include_start, not include_end)
            rq = self.idb_object_store.index(index_name).openCursor(bound)
            result = []

            def _idb_cb(event):
                cursor = event.target.result
                if cursor:
                    result.append(cursor.primaryKey)
                    __pragma__('js', 'cursor.continue();')
                else:
                    res(result)
            rq.onsuccess = _idb_cb
        return __new__(Promise(_resolution))


class IndexedDB:
    async def open(self, name: str, version: int, structure: dict):
        self.db = None
        self.structure = structure

        def _resolution(res, _):
            def _db_created(event):
                self.db = event.target.result
                self.db.onerror = self._on_db_error
                res()

            def _db_failed(event):
                console.log('DB open/create failed')

            rq = window.indexedDB.open(name, version)
            rq.onerror = _db_failed
            rq.onsuccess = _db_created
            rq.onupgradeneeded = self._on_open_upgrade_needed
        return __new__(Promise(_resolution))

    def _on_open_upgrade_needed(self, event):
        db = event.target.result
        for store_name, store_props in self.structure.items():
            store = db.createObjectStore(store_name, store_props['config'])

            for index_name, index_props in store_props['indexes'].items():
                index_path = index_name.split('-')
                if len(index_path) == 1:
                    index_path = index_path[0]
                store.createIndex(index_name, index_path, index_props)

    def _on_db_error(self, event):
        console.error("Database error: " + event.target.errorCode)

    def object_store(self, object_store: str, access_type: str = 'readonly'
                     ) -> IndexedDBTransaction:
        idb_t = self.db.transaction([object_store], access_type)

        def _on_complete():
            # console.log('transaction complete')
            pass
        idb_t.oncomplete = _on_complete
        return IndexedDBObjectStore(idb_t.objectStore(object_store))
