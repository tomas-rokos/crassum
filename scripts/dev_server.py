#!/usr/bin/env python3
from livereload import Server, shell
import os, os.path, glob
from subprocess import run

WATCH = [
    'frontend/*.py',
    'frontend/routes/*.py',
    'frontend/routes/**/*.py',
    'crassum/**/*.py',
]

RECOMPILE = 'frontend/app.py'


def recompile(to_compile):
    def inner():
        for fn in glob.glob(to_compile):
            run('transcrypt --map --build --nomin --esv 6 {}'.format(fn),
                shell=True, check=True)

    return inner


def start_server(to_watch, port=4444):
    # initial recompile
    recompile(RECOMPILE)()

    final_watch = []
    final_watch.extend(to_watch)
    final_watch.extend(WATCH)

    # watch .py files
    server = Server()
    for watch in final_watch:
        server.watch(watch, recompile(RECOMPILE))
    server.serve(root='.', port=port)
