from crassum.utils.random_string import random_string


def test_random_string_len():
    a = random_string(4)
    assert len(a) == 4

    b = random_string(6)
    assert len(b) == 6

    assert a != b
