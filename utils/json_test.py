from crassum.utils.json import get

sample = {
    'a': 'va',
    'b': {
        'ba': {
            'baa': 'vbaa',
            'bab': 'vbab'
        },
        'bb': 'vbb',
        5: 'vb5'
    }
}


def test_direct_subnode():
    assert get(sample, 'a') == 'va'


def test_non_existing_direct_subnode():
    assert get(sample, 'c') is None


def test_nested_subnode():
    assert get(sample, 'b.ba.baa') == 'vbaa'
    assert get(sample, 'b.bb') == 'vbb'
    assert get(sample, 'b.ba') == {'baa': 'vbaa', 'bab': 'vbab'}


def test_nested_numerical_key_subnode():
    assert get(sample, 'b.5') == 'vb5'


def test_non_existing_nested_subnode():
    assert get(sample, 'b.ba.baa.baaaa') is None
    assert get(sample, 'b.ba.bac') is None
    assert get(sample, 'b.bc') is None
    assert get(sample, 'c.cc') is None


def test_empty_key():
    assert get('ahoj', '') == 'ahoj'
    assert get({'c': 'ahoj'}, '') == {'c': 'ahoj'}

