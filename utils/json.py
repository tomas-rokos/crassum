def get(obj, path: str):
    path_keys = path.split('.')
    act = obj
    for key in path_keys:
        if key == '':
            continue
        if not isinstance(act, dict):
            return None
        try:
            new_act = act.get(int(key))
            if new_act is None:
                new_act = act.get(key)
        except:
            new_act = act.get(key)
        if new_act is None:
            return None
        act = new_act
    return act
