from datetime import datetime
from math import ceil


def get_now():
    return ceil(datetime.now().timestamp()*1000)
