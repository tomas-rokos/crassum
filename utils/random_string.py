from random import random


def random_string(chars: int):
    result = ''
    for single in range(chars):
        hit = int(random() * 36)
        if hit < 10:
            result += chr(hit + ord('0'))
        elif hit < 36:
            result += chr(hit + ord('a') - 10)
        else:
            result += '_'

    return result
