class Dispatcher:
    def __init__(self):
        self.listeners = {}

    def listen(self, event: str, fn):
        if event in self.listeners.keys():
            self.listeners[event].append(fn)
        else:
            self.listeners[event] = [fn]

    def stop_listen(self, event: str, fn):
        if fn in self.listeners[event]:
            self.listeners[event].remove(fn)

    def dispatch(self, event: str, data=None):
        for fn in self.listeners.get(event, []):
            fn(data)
