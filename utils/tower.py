_fns = []


def watch(fn):
    _fns.append(fn)


def stop(fn):
    _fns.remove(fn)


def send(signal):
    for fn in _fns:
        fn(signal)
