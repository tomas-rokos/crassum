class BBox:
    def __init__(self, origin, width: int, height: int):
        self.origin = [origin[0], origin[1]]
        self.width = width
        self.height = height

    @property
    def center(self):
        return [self.origin[0] + self.width / 2, self.origin[1] + self.height / 2]

    @property
    def end(self):
        return [self.origin[0] + self.width, self.origin[1] + self.height]

    @property
    def upper_right(self):
        return [self.origin[0] + self.width, self.origin[1]]

    @property
    def lower_left(self):
        return [self.origin[0], self.origin[1] + self.height]

    def extend(self, other):
        my_end = self.end
        other_end = other.end
        if self.origin[0] > other.origin[0]:
            self.origin[0] = other.origin[0]
        if self.origin[1] > other.origin[1]:
            self.origin[1] = other.origin[1]
        max_x = max(my_end[0], other_end[0])
        self.width = max_x - self.origin[0]
        max_y = max(my_end[1], other_end[1])
        self.height = max_y - self.origin[1]

    def enlarge(self, pxs):
        self.origin[0] -= pxs
        self.origin[1] -= pxs
        self.width += 2*pxs
        self.height += 2*pxs

    def __repr__(self):
        end = self.end
        return '[' + self.origin[0] + ', ' + self.origin[1] + ']' + \
            ' -> [' + end[0] + ', ' + end[1] + ']: (' + self.width + ', ' + \
            self.height + ')'
