from crassum.storage.blob_storage import BlobStorage


class BlobStorageMemory(BlobStorage):
    def __init__(self):
        self.data = {}

    async def get(self, blob_id: str):
        return self.data.get(blob_id)

    async def put(self, blob_id: str, blob):
        self.data[blob_id] = blob

    async def delete(self, blob_id: str):
        self.data.pop(blob_id)
