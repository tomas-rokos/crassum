from crassum.storage.blob_storage import BlobStorage


class BlobStorageLocalStorage(BlobStorage):
    async def get(self, blob_id: str):
        return localStorage.getItem(blob_id)

    async def put(self, blob_id: str, blob):
        localStorage.setItem(blob_id, blob)

    async def delete(self, blob_id: str):
        localStorage.removeItem(blob_id)
