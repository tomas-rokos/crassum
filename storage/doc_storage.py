
class DocStorageCollection:
    async def get_all(self) -> list:
        pass

    async def get(self, doc_id: str) -> dict:
        pass

    async def put(self, doc: dict):
        pass

    async def delete(self, doc_id: str):
        pass

    async def search(self, cond: dict):
        pass


class DocStorage:
    async def connect(self):
        pass

    def collection(self, name, writable=False) -> DocStorageCollection:
        pass
