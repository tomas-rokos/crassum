from copy import deepcopy

from crassum.storage.doc_storage import DocStorageCollection, DocStorage


class DocStorageCollectionMemory(DocStorageCollection):
    def __init__(self, collection: dict):
        self.collection = collection

    async def get_all(self) -> list:
        return [deepcopy(doc) for doc_id, doc in self.collection.items()]

    async def get(self, doc_id: str) -> dict:
        return deepcopy(self.collection.get(doc_id))

    async def put(self, doc: dict):
        self.collection[doc['_id']] = doc

    async def delete(self, doc_id: str):
        self.collection.pop(doc_id)

    async def search(self, cond: dict):
        result = []
        for doc_id, doc in self.collection.items():
            add_to_result = True
            for single_cond_prop, single_cond in cond.items():
                value = doc[single_cond_prop]
                if not isinstance(value, list):
                    value = [value]
                found = False
                for value_item in value:
                    if self._check_single_value(value_item, single_cond):
                        found = True
                        break
                if not found:
                    add_to_result = False
                    break
            if add_to_result:
                result.append(deepcopy(doc))
        return result

    @staticmethod
    def _check_single_value(value, cond):
        if isinstance(cond, dict):
            for cond_part_cond, cond_part_value in cond.items():
                if cond_part_cond == '$gte':
                    if value < cond_part_value:
                        return False
                elif cond_part_cond == '$lt':
                    if value >= cond_part_value:
                        return False
                elif cond_part_cond == '$lte':
                    if value > cond_part_value:
                        return False
                else:
                    raise NotImplementedError()
            return True
        else:
            return value == cond


class DocStorageMemory(DocStorage):
    def __init__(self):
        self.data = {}

    async def connect(self):
        pass

    def collection(self, name, writable=False) -> DocStorageCollection:
        collection = self.data.get(name)
        if collection is None:
            collection = {}
            self.data[name] = collection
        return DocStorageCollectionMemory(collection)
