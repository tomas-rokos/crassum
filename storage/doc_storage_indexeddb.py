from crassum.js.indexeddb import IndexedDB, IndexedDBObjectStore
from crassum.storage.doc_storage import DocStorageCollection, DocStorage


class DocStorageCollectionIndexedDB(DocStorageCollection):
    def __init__(self, object_store: IndexedDBObjectStore):
        self.object_store = object_store

    async def get_all(self) -> list:
        return await self.object_store.get_all()

    async def get(self, doc_id: str) -> dict:
        return await self.object_store.get(doc_id)

    async def put(self, doc: dict):
        await self.object_store.put(doc)

    async def delete(self, doc_id: str):
        await self.object_store.delete(doc_id)

    async def search(self, cond: dict):
        index_name = []
        start = []
        end = []
        include_start = True
        include_end = True
        for cond_prop_name, cond_value in cond.items():
            index_name.append(cond_prop_name)
            if isinstance(cond_value, dict):
                for cond_part_cond, cond_part_value in cond_value.items():
                    if cond_part_cond == '$gte':
                        start.append(cond_part_value)
                    elif cond_part_cond == '$lt':
                        end.append(cond_part_value)
                        include_end = False
                    elif cond_part_cond == '$lte':
                        end.append(cond_part_value)
                    else:
                        raise NotImplementedError()
            else:
                start.append(cond_value)
                end.append(cond_value)
        if len(index_name) == 1:
            start = start[0]
            end = end[0]
        index_name = '-'.join(index_name)
        return await self.object_store.get_range(index_name, start, include_start,
                                                 end, include_end)


class DocStorageIndexedDB(DocStorage):
    def __init__(self, name, version, definition):
        self.name = name
        self.version = version
        self.definition = definition
        self.indexed_db = IndexedDB()

    async def connect(self):
        await self.indexed_db.open(self.name, self.version, self.definition)

    def collection(self, name, writable=False) -> DocStorageCollection:
        mode = 'readwrite' if writable else 'readonly'
        s = self.indexed_db.object_store(name, mode)
        return DocStorageCollectionIndexedDB(s)
