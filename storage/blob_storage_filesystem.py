import os

from crassum.storage.blob_storage import BlobStorage


class BlobStorageFilesystem(BlobStorage):
    def __init__(self):
        self.stream_files = {}

    async def get(self, blob_id: str):
        try:
            with open(self.stream_files[blob_id]) as f:
                content = f.read()
        except FileNotFoundError:
            content = None
        return content

    async def put(self, blob_id: str, blob):
        with open(self.stream_files[blob_id], 'w') as f:
            f.write(blob)

    async def delete(self, blob_id: str):
        os.remove(self.stream_files[blob_id])
