# __pragma__('skip')


# __pragma__('noskip')

def bootstrap(app, wait_for_load=True):
    # Render the component in a 'container' div
    def main():
        # using React.createElement directly
        container = document.getElementById('container')
        root = ReactDOM.createRoot(container)
        root.render(React.createElement(app))

    if wait_for_load:
        # run main() when the document is ready
        document.addEventListener("DOMContentLoaded", main)
    else:
        main()


# __pragma__('skip')


def bootstrap(app):
    pass
