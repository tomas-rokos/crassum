from crassum.js.register_style import register_style


def _element(elem, props, *children):
    '''Creates React elements using Component class and properties dictionary'''
    return React.createElement(elem, props, *children)


def pyreact_sprite(item):
    content = None
    attribs = None
    item_len = len(item)
    if item_len > 2:
        content = _sprite_content(item[2])
    if item_len > 1:
        attribs = item[1]
        if isinstance(attribs, str):
            attribs = {'className': attribs}
    if attribs and attribs.get('class'):
        val = attribs.pop('class')
        if attribs.get('className', '') == '':
            attribs['className'] = ''
        attribs['className'] += ' ' + register_style(val)
    if content is None:
        return _element(item[0], attribs)
    else:
        return _element(item[0], attribs, *content)


def _sprite_content(content):
    if isinstance(content, tuple):
        return [pyreact_sprite(content)]
    if isinstance(content, list):
        result = []
        for content_item in content:
            if isinstance(content_item, tuple):
                result.append(pyreact_sprite(content_item))
            else:
                result.append(content_item)
        return result
    return [content]


def use_state(default_value):
    return React.useState(default_value)


def use_effect(fn, cond=None):
    return React.useEffect(fn, cond)


def use_ref(default_value):
    return React.useRef(default_value)


def use_async(fn, cond=None):
    result, set_result = use_state(None)

    def _get_result():
        async def _impl():
            store_result = await fn()
            set_result(store_result)

        _impl()

    use_effect(_get_result, cond)
    return result
