from crassum.js.navigation import on_navigation, get_location
from crassum.react.pyreact import Component


class RouterQuery(Component):
    def __init__(self, props):
        super().__init__(props)
        self.custom_props = props
        on_navigation(self._on_navigation_change)
        self.state = {'query': self._parse_seach(get_location().search)}

    def _on_navigation_change(self, loc):
        self.setState({'query': self._parse_seach(loc.search)})

    def _parse_seach(self, search: str):
        if len(search) == 0:
            return {}
        search = search[1:]
        search = search.split('&')
        result = {}
        for single in search:
            parts = single.split('=')
            result[parts[0]] = parts[1]
        return result

    def render(self):
        comp = self.props['']
        for key in self.state['query'].keys():
            test = self.props[key]
            if not test:
                continue
            comp = test
            break
        return self.sprite((comp, {'query': self.state['query']}))
