from math import sqrt, atan

from crassum.react.pyreact import Component


class Line(Component):
    def __init__(self, props):
        super().__init__(props)

    def render(self):
        style = {}
        origin, end = self.props['from'], self.props['to']
        if self.props['from'][0] > self.props['to'][0]:
            origin, end = self.props['to'], self.props['from']
        len_x = end[0]-origin[0]
        len_y = end[1]-origin[1]
        length = sqrt(len_x*len_x + len_y*len_y)
        style = {'left': origin[0], 'top': origin[1], 'width': length}
        if origin[1] != end[1]:
            angle = atan((end[1] - origin[1])/(end[0] - origin[0]))
            style['transform'] = f'rotate({angle}rad)'
        return self.sprite(
            ('div', {'className': 'line', 'style': style})
        )
