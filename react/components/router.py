from crassum.js.dict_keys import dict_keys
from crassum.js.navigation import on_navigation, get_location
from crassum.react.pyreact_functional import pyreact_sprite, use_state


def router(props):
    curr_path, set_curr_path = use_state(get_location().pathname)

    def _on_navigation_change(loc):
        set_curr_path(loc.pathname)

    on_navigation(_on_navigation_change)

    parts = curr_path.split('#')[0].split('/')
    len_parts = len(parts)
    patterns = dict_keys(props)
    match = props['']
    match_props = {}
    for pattern in patterns:
        pattern_parts = pattern.split('/')
        if len_parts != len(pattern_parts):
            continue
        found = True
        for idx in range(len_parts):
            pattern_part = pattern_parts[idx]
            len_pattern_part = len(pattern_part)
            if pattern_part[0] == '{' and pattern_part[len_pattern_part-1] == '}':
                variable = pattern_part[1:len_pattern_part-1]
                match_props[variable] = parts[idx]
                continue

            if parts[idx] != pattern_parts[idx]:
                found = False
                break
        if not found:
            match_props = {}
            continue
        match = props[pattern]
        break

    return pyreact_sprite((match, {'query': match_props}))
