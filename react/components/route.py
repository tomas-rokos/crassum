from crassum.js.navigation import navigate
from crassum.react.pyreact_functional import pyreact_sprite


def Route(props: dict):
    def _on_click(handler):
        navigate(props['href'])
        handler.stopPropagation()
        handler.preventDefault()
        return False

    return pyreact_sprite(('a',
                           {'href': props['href'],
                            'onClick': _on_click,
                            'className': props['className']},
                           props.children))
