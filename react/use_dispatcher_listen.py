from crassum.react.pyreact_functional import use_effect
from crassum.utils.dispatcher import Dispatcher


def use_dispatcher_listen(dispatcher: Dispatcher, event_name: str, fn):
    def _register_listeners():
        dispatcher.listen(event_name, fn)

        def _unregister_listeners():
            dispatcher.stop_listen(event_name, fn)

        return _unregister_listeners

    use_effect(_register_listeners, [])
