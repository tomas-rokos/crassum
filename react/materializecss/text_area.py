from crassum.react.pyreact_functional import pyreact_sprite, use_effect, \
    use_ref


# https://materializecss.com/text-inputs.html


def text_area(props: dict):
    input_ref = use_ref(None)

    def _on_input(e):
        props['onValue'](props['form_id'], props['id'], e.currentTarget.value)

    def _fn():
        M.updateTextFields()
        M.textareaAutoResize(input_ref.current)
        props['onValue'](props['form_id'], props['id'], props['default'])

    use_effect(_fn, [props['default']])

    return pyreact_sprite(
        ('div', {'className': 'input-field ' + props['className']}, [
            ('textarea',  {'id': props['id'],
                           'className': 'materialize-textarea',
                           'onInput': _on_input,
                           'ref': input_ref,
                           'defaultValue': props['default']}),
            ('label',  {'for': props['id']}, props['label'])
        ])
    )
