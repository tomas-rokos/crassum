from crassum.react.pyreact_functional import pyreact_sprite, use_ref, use_effect, \
    use_state

# https://materializecss.com/text-inputs.html


# language=css
STYLE = '''
:host {
}
'''


def select(props: dict):
    input_ref = use_ref(None)

    multiple = props['multiple'] is True

    def _on_input(e):
        if multiple:
            ref = M.FormSelect.getInstance(input_ref.current)
            val = ref.getSelectedValues()
        else:
            val = e.currentTarget.value
        props['onValue'](props['form_id'], props['id'], val)

    def _fn():
        inst = M.FormSelect.init(input_ref.current, {'isMultiple': True})
        for child in props['children']:
            if child['props']['selected'] == '1':
                props['onValue'](props['form_id'], props['id'], child['props']['value'])

    use_effect(_fn, [])

    class_names = 'input-field ' + props['className']
    return pyreact_sprite(
        ('div', {'className': class_names, 'class': STYLE}, [
            ('select', {'id': props['id'],
                        'onChange': _on_input,
                        'ref': input_ref,
                        'multiple': multiple,
                        }, props['children']),
            ('label',  {'for': props['id'], 'onClick': _on_input}, props['label'])
        ])
    )
