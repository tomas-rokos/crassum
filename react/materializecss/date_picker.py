from crassum.react.pyreact_functional import pyreact_sprite, use_ref, use_effect, \
    use_state

# https://materializecss.com/text-inputs.html


# language=css
STYLE = '''
:host {
}
'''


def date_picker(props: dict):
    input_ref = use_ref(None)

    def _on_input(e):
        val = e.currentTarget.value
        props['onValue'](props['form_id'], props['id'], val)

    def _on_close(e):
        val = input_ref.current.value
        props['onValue'](props['form_id'], props['id'], val)

    def _fn():
        inst = M.Datepicker.init(input_ref.current, {
            'firstDay': 1,
            'format': 'yyyy-mm-dd',
            'i18n': {
                'cancel': 'Zrušit',
                'done': 'Použít',
                'months': ['Leden', 'Únor', 'Březen', 'Duben', 'Květen',
                           'Červen', 'Červenec', 'Srpen', 'Září', 'Řijen',
                           'Listopad', 'Prosinec'],
                'monthsShort': ['Led', 'Únor', 'Bře', 'Dub', 'Kvě', 'Čer',
                                'Čvnc', 'Srp', 'Zář', 'Říj', 'Lis', 'Pro'],
                'weekdays': ['Neděle', 'Pondělí', 'Úterý', 'Středa', 'Čtvrtek',
                             'Pátek', 'Sobota'],
                'weekdaysShort': ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So'],
                'weekdaysAbbrev': ['N', 'P', 'Ú', 'S', 'Č', 'P', 'S'],
            },
            'onClose': _on_close
        })

    use_effect(_fn, [])

    class_names = 'input-field ' + props['className']
    return pyreact_sprite(
        ('div', {'className': class_names, 'class': STYLE}, [
            ('input',  {'id': props['id'],
                        'type': 'text',
                        'onInput': _on_input,
                        'ref': input_ref}),
            ('label',  {'for': props['id']}, props['label'])
        ])
    )
