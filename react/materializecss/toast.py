def toast(content: str, delay = 2000):
    M.toast({'html': content, 'displayLength': delay})
