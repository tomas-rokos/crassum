from crassum.react.pyreact_functional import pyreact_sprite, use_ref, use_effect, \
    use_state

# https://materializecss.com/text-inputs.html


# language=css
STYLE = '''
:host {
}

:host .downloading {
    background-color: lightgrey !important;
}
'''


class AutocompleteDataSource:
    def initial_data(self) -> dict:
        pass

    def has_dynamic_load(self):
        return False

    def load_dynamic_data(self, text, cb):
        pass


def autocomplete(props: dict):
    data_source: AutocompleteDataSource = props['dataSource']
    my_state, set_my_state = use_state('')
    initiated, set_initiated = use_state(False)

    input_ref = use_ref(None)

    def _on_new_data(data):
        inst = M.Autocomplete.getInstance(input_ref.current)
        inst.updateData(data)
        inst.open()
        set_my_state('')

    def _on_input(e):
        val = e.currentTarget.value
        props['onValue'](props['form_id'], props['id'], val)
        if len(val) < 3 or len(val) > 10:
            return
        if initiated is False:
            _on_new_data(data_source.initial_data())
            set_initiated(True)
        if data_source.has_dynamic_load() is False:
            return
        set_my_state(' downloading')
        data_source.load_dynamic_data(val, _on_new_data)

    def _on_autocomplete(e):
        props['onValue'](props['form_id'], props['id'], e)

    def _fn():
        inst = M.Autocomplete.init(input_ref.current, {
            'data': data_source.initial_data(),
            'onAutocomplete': _on_autocomplete,
        })

    use_effect(_fn, [])

    class_names = 'input-field ' + props['className']
    return pyreact_sprite(
        ('div', {'className': class_names, 'class': STYLE}, [
            ('input',  {'id': props['id'],
                        'type': 'text',
                        'onInput': _on_input,
                        'ref': input_ref,
                        'className': my_state,
                        'autocomplete': 'off'}),
            ('label',  {'for': props['id']}, props['label'])
        ])
    )
