from crassum.react.pyreact_functional import pyreact_sprite

# https://materializecss.com


def button(props: dict):
    def _on_click(e):
        cb = props['onMsg']
        if cb:
            cb(props['form_id'], props['id'])

    return pyreact_sprite(
        ('a', {
            'className': 'waves-effect waves-light btn ' + props['className'],
            'onClick': _on_click},
         props['children'])
    )
