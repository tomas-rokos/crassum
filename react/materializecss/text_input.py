from crassum.react.pyreact_functional import pyreact_sprite, use_effect


# https://materializecss.com/text-inputs.html


def text_input(props: dict):
    def _on_input(e):
        props['onValue'](props['form_id'], props['id'], e.currentTarget.value)

    def _fn():
        M.updateTextFields()
        props['onValue'](props['form_id'], props['id'], props['default'])

    use_effect(_fn, [props['default']])

    input_type = 'text'
    if props['type']:
        input_type = props['type']
    dirty_hash = ''
    if 'defaultHash' in props:
        dirty_hash = props['defaultHash']
    return pyreact_sprite(
        ('div', {'className': 'input-field ' + props['className']}, [
            ('input',  {'id': props['id'], 'key': props['default'] + dirty_hash,
                        'type': input_type, 'onInput': _on_input,
                        'defaultValue': props['default']}),
            ('label',  {'for': props['id']}, props['label'])
        ])
    )
