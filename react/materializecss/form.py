from crassum.react.pyreact_functional import pyreact_sprite, use_effect, use_state

form_values = {}


def form(props: dict):
    global form_values

    init, set_init = use_state(True)
    if init:
        form_values[props['id']] = {}
        set_init(False)

    def _on_value(form_id: str, child_id: str, val):
        if not form_values[props['id']]:
            form_values[props['id']] = {}
        form_values[form_id][child_id] = val
        cb = props['onValue']
        if cb:
            cb(child_id, form_values[form_id])

    def _on_msg(form_id: str, child_id: str):
        cb = props['onMsg']
        if cb:
            cb(child_id, form_values[form_id])

    for child in props['children']:
        child['props']['form_id'] = props['id']
        child['props']['onValue'] = _on_value
        child['props']['onMsg'] = _on_msg

    return pyreact_sprite(
        ('div', {'className': props['className']}, props['children'])
    )
