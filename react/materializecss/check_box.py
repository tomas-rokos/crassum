from crassum.react.pyreact_functional import pyreact_sprite, use_effect


# https://materializecss.com


def check_box(props: dict):
    def _on_value(e):
        props['onValue'](props['form_id'], props['id'], e.currentTarget.checked)

    checked = props['checked'] is True

    def _fn():
        props['onValue'](props['form_id'], props['id'], checked)

    use_effect(_fn, [])

    return pyreact_sprite(
        ('p', {'className': props['className']}, [
            ('label', { 'className': props['className']}, [
                ('input', {'type': 'checkbox', 'onChange': _on_value, 'defaultChecked': checked}),
                ('span', None, props['label']),
            ]),
        ])
    )
