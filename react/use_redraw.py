from crassum.react.pyreact_functional import use_state


def use_redraw():
    count, set_count = use_state(0)

    def _fn():
        set_count(count + 1)
    return _fn
