
connect = ReactRedux.connect
use_dispatch = ReactRedux.useDispatch
use_selector = ReactRedux.useSelector
provider = ReactRedux.Provider

_PSEUDO_EPICS = {}


def _my_pseudo_epic(store):
    def _wrap_dispatch(redux_next):
        def _handle_action(action):
            global _PSEUDO_EPICS
            epic = _PSEUDO_EPICS and _PSEUDO_EPICS[action['type']]
            if epic:
                setTimeout(lambda: epic(action, store.dispatch, store.getState), 1)
            return redux_next(action)
        return _handle_action
    return _wrap_dispatch


def _defaulter(fn):
    def _fn(state, action):
        if not state:
            state = {}
        return fn(state, action)
    return _fn


def create_store(structure, pseudo_epics=None, use_dev_tools=True):
    global _PSEUDO_EPICS
    for single_epics in pseudo_epics:
        _PSEUDO_EPICS.update(single_epics)
    compose = Redux.compose
    if use_dev_tools:
        compose = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    fixed_structure = {key: _defaulter(fn) for key, fn in structure.items()}
    return Redux.createStore(
        Redux.combineReducers(fixed_structure),
        compose(Redux.applyMiddleware(_my_pseudo_epic)),
    )


def update_state(current, updates):
    return Object.assign({}, current, updates)
