from crassum.react.pyreact_functional import pyreact_sprite, use_effect, use_state

# language=css
STYLE = '''
:host {
    font-weight: bold;
    font-size: 20px;
}
'''


def contact(props):
    count, set_count = use_state(0)

    def _my_effect():
        console.log('in effect')

        def _fn():
            set_count(count + 1)
        setTimeout(_fn, 200)

        def _cleanup():
            console.log('in cleanup')

        return _cleanup

    use_effect(_my_effect, [])

    def _on_click(e):
        set_count(count + 1)
        print('click', e)

    return pyreact_sprite(
        ('div', {'class': STYLE, 'onClick': _on_click}, [
            ('div', None, ['CONTACT', str(count)]),
            ('div', None, ['CONTACT', str(count)]),
        ])
    )
