from crassum.react.react_redux import create_store


def first_reducer(state, action):
    if not state:
        return {'abc': 0}
    if action.type == 'abc':
        return {'abc': 1}
    else:
        return state


def second_reducer(state, action):
    if not state:
        return {'def': 0}
    if action.type == 'def':
        return {'def': 1}
    else:
        return state


store = create_store({'first': first_reducer, 'second': second_reducer})
