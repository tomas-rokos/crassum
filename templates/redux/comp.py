from crassum.react.pyreact_functional import pyreact_sprite, use_effect, use_state
from crassum.react.react_redux import connect


# language=css
STYLE = '''
:host {
    font-weight: bold;
    font-size: 20px;
}
'''


def _comp(props):
    console.log(props)
    return pyreact_sprite(
        ('div', {'class': STYLE}, [
            'test'
        ])
    )


def _map_state_to_props(state):
    console.log(state)
    return {}


def _map_dispatch_to_props(dispatch):
    console.log(dispatch)
    return {}


comp = connect(_map_state_to_props, _map_dispatch_to_props)(_comp)
